//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Testv1;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class User_Login extends JFrame {
    private JPanel contentPane;
    private JTextField textField;
    private JPasswordField passwordField;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    User_Login frame = new User_Login();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public User_Login() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 450, 300);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JLabel lblNewLabel = new JLabel("读者登录");
        lblNewLabel.setFont(new Font("宋体", 0, 22));
        lblNewLabel.setBounds(170, 10, 115, 36);
        this.contentPane.add(lblNewLabel);
        JLabel lblNewLabel_1 = new JLabel("借书证号：");
        lblNewLabel_1.setFont(new Font("宋体", 0, 14));
        lblNewLabel_1.setBounds(44, 64, 84, 36);
        this.contentPane.add(lblNewLabel_1);
        JLabel lblNewLabel_2 = new JLabel("密码：");
        lblNewLabel_2.setFont(new Font("宋体", 0, 14));
        lblNewLabel_2.setBounds(59, 126, 42, 15);
        this.contentPane.add(lblNewLabel_2);
        this.textField = new JTextField();
        this.textField.setBounds(138, 72, 184, 21);
        this.contentPane.add(this.textField);
        this.textField.setColumns(10);
        JButton btnNewButton = new JButton("返回");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                User_Login.this.setVisible(false);
            }
        });
        btnNewButton.setBounds(309, 197, 93, 23);
        this.contentPane.add(btnNewButton);
        this.passwordField = new JPasswordField();
        this.passwordField.setBounds(138, 123, 184, 18);
        this.contentPane.add(this.passwordField);
        final JLabel lblNewLabel_3 = new JLabel("借书证号或密码错误，请重新输入");
        lblNewLabel_3.setForeground(Color.RED);
        lblNewLabel_3.setBounds(138, 163, 198, 24);
        this.contentPane.add(lblNewLabel_3);
        lblNewLabel_3.setVisible(false);
        JButton btnNewButton_1 = new JButton("登录");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String strP = new String(User_Login.this.passwordField.getPassword());
                String strT = new String(User_Login.this.textField.getText());
                String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                String userName = "root";
                String userPwd = "ys124126";
                Connection dbConn = null;
                String sql = "select 借书证号,密码 from 借书者";

                try {
                    Statement state = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    state = dbConn.createStatement();
                    ResultSet rs = state.executeQuery(sql);

                    while(true) {
                        while(rs.next()) {
                            if (strT.equals(rs.getString(1)) && strP.equals(rs.getString(2))) {
                                lblNewLabel_3.setVisible(false);
                                U_mianPage ump = new U_mianPage();
                                ump.setVisible(true);
                                Calendar c = Calendar.getInstance();
                                PreparedStatement pst = null;
                                pst = dbConn.prepareStatement("insert into 操作账号 values(?,?)");
                                pst.setString(1, strT);
                                pst.setTimestamp(2, new Timestamp(c.getTimeInMillis()));
                                pst.addBatch();
                                pst.executeBatch();
                            } else {
                                lblNewLabel_3.setVisible(true);
                            }
                        }

                        dbConn.close();
                        break;
                    }
                } catch (SQLException var14) {
                    var14.printStackTrace();
                } catch (ClassNotFoundException var15) {
                    var15.printStackTrace();
                }

            }
        });
        btnNewButton_1.setBounds(8, 197, 93, 23);
        this.contentPane.add(btnNewButton_1);
    }
}
