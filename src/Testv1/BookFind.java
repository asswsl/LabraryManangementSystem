//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//
//查找图书
package Testv1;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class BookFind extends JFrame {
    private JPanel contentPane;
    private JTextField textField_2;
    private JTextField textField_3;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    BookFind frame = new BookFind();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public BookFind() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 581, 391);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        this.textField_2 = new JTextField();
        this.textField_2.setBounds(23, 7, 139, 21);
        this.contentPane.add(this.textField_2);
        this.textField_2.setColumns(10);
        JButton btnNewButton_2 = new JButton("返回");
        btnNewButton_2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BookFind.this.setVisible(false);
            }
        });
        btnNewButton_2.setBounds(462, 319, 93, 23);
        this.contentPane.add(btnNewButton_2);
        JLabel lblNewLabel = new JLabel("共有（本）");
        lblNewLabel.setBounds(23, 323, 70, 15);
        this.contentPane.add(lblNewLabel);
        this.textField_3 = new JTextField();
        this.textField_3.setBounds(82, 320, 66, 21);
        this.contentPane.add(this.textField_3);
        this.textField_3.setColumns(10);
        JPanel panel = new JPanel();
        panel.setBounds(23, 38, 532, 271);
        this.contentPane.add(panel);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 46, 300, 150);
        panel.add(scrollPane);
        final String[] Name = new String[]{"书号", "书名", "作者", "出版社", "定价", "内容简介", "状态", "分类"};
        final Object[][] rowData = new Object[100][8];
        final JTable table = new JTable(rowData, Name);
        table.setBounds(297, 179, -279, -124);
        table.setRowHeight(30);
        table.getColumnModel().getColumn(0).setPreferredWidth(110);
        table.setPreferredScrollableViewportSize(new Dimension(500, 300));
        scrollPane.setViewportView(table);

        try {
            String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
            String userName = "root";
            String userPwd = "ys124126";
            Connection dbConn = null;
            Statement state = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
            state = dbConn.createStatement();
            String sql0 = "select * from 图书";
            ResultSet rs = state.executeQuery(sql0);
            Statement stateCnt = dbConn.createStatement();
            String sql1 = "select count(*) from 图书";
            ResultSet rs_1 = stateCnt.executeQuery(sql1);

            while(rs_1.next()) {
                this.textField_3.setText(rs_1.getString(1));
            }

            for(int i = 0; rs.next() && i < rowData.length; ++i) {
                rowData[i][0] = rs.getString(1);
                rowData[i][1] = rs.getString(2);
                rowData[i][2] = rs.getString(3);
                rowData[i][3] = rs.getString(4);
                rowData[i][4] = rs.getString(5);
                rowData[i][5] = rs.getString(6);
                rowData[i][6] = rs.getString(7);
                rowData[i][7] = rs.getString(8);
            }

            dbConn.close();
        } catch (SQLException var19) {
            var19.printStackTrace();
        } catch (ClassNotFoundException var20) {
            var20.printStackTrace();
        }

        String[] item = new String[]{"书号", "书名", "作者", "出版社", "定价", "内容简介", "状态", "分类"};
        final JComboBox comboBox = new JComboBox(item);
        comboBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
            }
        });
        comboBox.setBounds(172, 6, 139, 23);
        this.contentPane.add(comboBox);
        JButton btnNewButton = new JButton("查询");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = new String(BookFind.this.textField_2.getText());
                String str_1 = new String((String)comboBox.getSelectedItem());

                try {
                    String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                    String userName = "root";
                    String userPwd = "ys124126";
                    Connection dbConn = null;
                    Statement state = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    state = dbConn.createStatement();
                    String sql0 = "select * from 图书 where " + str_1 + "='" + str + "'";
                    ResultSet rs = state.executeQuery(sql0);
                    Statement stateCnt = dbConn.createStatement();
                    String sql1 = "select count(*) from 图书 where " + str_1 + "='" + str + "'";
                    ResultSet rs_1 = stateCnt.executeQuery(sql1);

                    int n;
                    for(n = 0; rs_1.next(); n = Integer.parseInt(rs_1.getString(1))) {
                        BookFind.this.textField_3.setText(rs_1.getString(1));
                    }

                    int i;
                    for(i = 0; rs.next() && i < rowData.length; ++i) {
                        rowData[i][0] = rs.getString(1);
                        rowData[i][1] = rs.getString(2);
                        rowData[i][2] = rs.getString(3);
                        rowData[i][3] = rs.getString(4);
                        rowData[i][4] = rs.getString(5);
                        rowData[i][5] = rs.getString(6);
                        rowData[i][6] = rs.getString(7);
                        rowData[i][7] = rs.getString(8);
                    }

                    while(i >= n && i < rowData.length) {
                        rowData[i][0] = null;
                        rowData[i][1] = null;
                        rowData[i][2] = null;
                        rowData[i][3] = null;
                        rowData[i][4] = null;
                        rowData[i][5] = null;
                        rowData[i][6] = null;
                        rowData[i][7] = null;
                        ++i;
                    }

                    TableModel tml = new DefaultTableModel(rowData, Name);
                    table.setModel(tml);
                    dbConn.close();
                } catch (SQLException var17) {
                    var17.printStackTrace();
                } catch (ClassNotFoundException var18) {
                    var18.printStackTrace();
                }

            }
        });
        btnNewButton.setBounds(363, 6, 93, 23);
        this.contentPane.add(btnNewButton);
        JButton btnNewButton_1 = new JButton("取消");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                    String userName = "root";
                    String userPwd = "ys124126";
                    Connection dbConn = null;
                    Statement state = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    state = dbConn.createStatement();
                    String sql0 = "select * from 图书";
                    ResultSet rs = state.executeQuery(sql0);
                    Statement stateCnt = dbConn.createStatement();
                    String sql1 = "select count(*) from 图书";
                    ResultSet rs_1 = stateCnt.executeQuery(sql1);

                    while(rs_1.next()) {
                        BookFind.this.textField_3.setText(rs_1.getString(1));
                    }

                    for(int i = 0; rs.next() && i < rowData.length; ++i) {
                        rowData[i][0] = rs.getString(1);
                        rowData[i][1] = rs.getString(2);
                        rowData[i][2] = rs.getString(3);
                        rowData[i][3] = rs.getString(4);
                        rowData[i][4] = rs.getString(5);
                        rowData[i][5] = rs.getString(6);
                        rowData[i][6] = rs.getString(7);
                        rowData[i][7] = rs.getString(8);
                    }

                    TableModel tml = new DefaultTableModel(rowData, Name);
                    table.setModel(tml);
                    dbConn.close();
                } catch (SQLException var14) {
                    var14.printStackTrace();
                } catch (ClassNotFoundException var15) {
                    var15.printStackTrace();
                }

            }
        });
        btnNewButton_1.setBounds(462, 7, 93, 23);
        this.contentPane.add(btnNewButton_1);
    }
}
