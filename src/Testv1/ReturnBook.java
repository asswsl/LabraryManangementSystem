//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Testv1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class ReturnBook extends JFrame {
    private JPanel contentPane;
    private JTextField textField;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ReturnBook frame = new ReturnBook();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public ReturnBook() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 543, 428);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JLabel lblNewLabel = new JLabel("书号");
        lblNewLabel.setBounds(10, 306, 54, 15);
        this.contentPane.add(lblNewLabel);
        this.textField = new JTextField();
        this.textField.setBounds(54, 303, 107, 21);
        this.contentPane.add(this.textField);
        this.textField.setColumns(10);
        final JLabel lblNewLabel_1 = new JLabel("操作成功");
        lblNewLabel_1.setBounds(107, 360, 54, 15);
        this.contentPane.add(lblNewLabel_1);
        lblNewLabel_1.setVisible(false);
        JButton btnNewButton_1 = new JButton("返回");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ReturnBook.this.setVisible(false);
            }
        });
        btnNewButton_1.setBounds(408, 356, 93, 23);
        this.contentPane.add(btnNewButton_1);
        final JLabel lblNewLabel_2 = new JLabel("操作失败");
        lblNewLabel_2.setForeground(Color.RED);
        lblNewLabel_2.setBounds(34, 331, 54, 15);
        this.contentPane.add(lblNewLabel_2);
        lblNewLabel_2.setVisible(false);
        JPanel panel = new JPanel();
        panel.setBounds(10, 10, 507, 286);
        this.contentPane.add(panel);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 46, 300, 150);
        panel.add(scrollPane);
        final String[] Name = new String[]{"书号", "书名", "借出时间"};
        final Object[][] rowData = new Object[100][3];
        final JTable table = new JTable(rowData, Name);
        table.setBounds(297, 179, -279, -124);
        table.setRowHeight(30);
        table.getColumnModel().getColumn(0).setPreferredWidth(110);
        table.setPreferredScrollableViewportSize(new Dimension(480, 300));
        scrollPane.setViewportView(table);
        String user = null;

        try {
            String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
            String userName = "root";
            String userPwd = "ys124126";
            Connection dbConn = null;
            Statement state = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
            state = dbConn.createStatement();
            String sql0 = "select 操作账号,时间 from 操作账号 order by 时间 desc limit 1";
            ResultSet rs = state.executeQuery(sql0);

            String s1;
            for(s1 = new String(); rs.next(); s1 = rs.getString(1)) {
            }

            Statement state_1 = dbConn.createStatement();
            String sql1 = "select * from 借书 where 借书证号 = '" + s1 + "' order by 借书时间 asc";
            ResultSet rs_1 = state_1.executeQuery(sql1);

            for(int i = 0; rs_1.next() && i < rowData.length; ++i) {
                rowData[i][0] = rs_1.getString("书号");
                rowData[i][1] = rs_1.getString("书名");
                rowData[i][2] = rs_1.getString("借书时间");
            }

            dbConn.close();
        } catch (SQLException var23) {
            var23.printStackTrace();
        } catch (ClassNotFoundException var24) {
            var24.printStackTrace();
        }

        final JLabel lblNewLabel_3 = new JLabel("已超时还书，请到前台缴滞纳金");
        lblNewLabel_3.setForeground(Color.RED);
        lblNewLabel_3.setBounds(182, 331, 184, 15);
        this.contentPane.add(lblNewLabel_3);
        lblNewLabel_3.setVisible(false);
        JButton btnNewButton = new JButton("还书");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String Bnum = new String(ReturnBook.this.textField.getText());
                Calendar c = Calendar.getInstance();
                String tm = null;
                long result = 0L;

                String dbURL;
                String userName;
                String userPwd;
                Connection dbConn;
                Statement pst1;
                String Unum;
                try {
                     dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                     userName = "root";
                     userPwd = "ys124126";
                    dbConn = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    pst1 = dbConn.createStatement();
                    Unum = "select 借书时间 from 借书 where 书号 = '" + Bnum + "'";

                    for(ResultSet rs_1x = pst1.executeQuery(Unum); rs_1x.next(); tm = rs_1x.getString(1)) {
                    }

                    Calendar ct = Calendar.getInstance();
                    Timestamp ts = Timestamp.valueOf(tm);
                    ct.setTime(ts);
                    result = (c.getTimeInMillis() - ct.getTimeInMillis()) / 86400000L;
                } catch (SQLException var36) {
                    var36.printStackTrace();
                    lblNewLabel_2.setVisible(true);
                } catch (ClassNotFoundException var37) {
                    var37.printStackTrace();
                    lblNewLabel_2.setVisible(true);
                }

                if (result > 30L) {
                    lblNewLabel_3.setVisible(true);
                    lblNewLabel_2.setVisible(true);
                    lblNewLabel_1.setVisible(false);
                } else {
                    lblNewLabel_3.setVisible(false);
                    lblNewLabel_2.setVisible(false);

                    try {
                         dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                         userName = "root";
                         userPwd = "ys124126";
                        dbConn = null;
                        Class.forName("com.mysql.cj.jdbc.Driver");
                        dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                        pst1 = null;
                        PreparedStatement pst1x = dbConn.prepareStatement("update 图书 set 状态=? where 书号=?");
                        pst1x.setString(1, "空闲在册");
                        pst1x.setString(2, Bnum);
                        pst1x.addBatch();
                        pst1x.executeBatch();
                        Unum = null;
                        Statement state_1 = dbConn.createStatement();
                        String sql1 = "select * from 借书 where 书号='" + Bnum + "'";
                        ResultSet rs1 = state_1.executeQuery(sql1);

                        String s1;
                        for(s1 = new String(); rs1.next(); s1 = rs1.getString("借书证号")) {
                        }

                        String name = null;
                        Statement state_2 = dbConn.createStatement();
                        String sql2 = "select * from 借书 where 书号='" + Bnum + "'";
                        ResultSet rs2 = state_2.executeQuery(sql2);

                        String s2;
                        for(s2 = new String(); rs2.next(); s2 = rs2.getString("姓名")) {
                        }

                        String Bname = null;
                        Statement state_3 = dbConn.createStatement();
                        String sql3 = "select * from 借书 where 书号='" + Bnum + "'";

                        for(ResultSet rs3 = state_3.executeQuery(sql3); rs3.next(); Bname = rs3.getString("书名")) {
                        }

                        PreparedStatement pst2 = null;
                        pst2 = dbConn.prepareStatement("insert into 还书 values(?,?,?,?,?,?)");
                        pst2.setString(1, s1);
                        pst2.setString(2, s2);
                        pst2.setString(3, Bnum);
                        pst2.setString(4, Bname);
                        pst2.setString(5, tm);
                        pst2.setTimestamp(6, new Timestamp(c.getTimeInMillis()));
                        pst2.addBatch();
                        pst2.executeBatch();
                        String sql0 = "delete from 借书 where 书号= ?";
                        PreparedStatement pst = dbConn.prepareStatement(sql0);
                        pst.setString(1, Bnum);
                        pst.addBatch();
                        pst.executeBatch();
                        Statement state_1_1 = dbConn.createStatement();
                        String sql1_1 = "select * from 借书 where 借书证号 = '" + s1 + "' order by 借书时间 asc";
                        ResultSet rs_1 = state_1_1.executeQuery(sql1_1);

                        int i;
                        for(i = 0; rs_1.next() && i < rowData.length; ++i) {
                            rowData[i][0] = rs_1.getString("书号");
                            rowData[i][1] = rs_1.getString("书名");
                            rowData[i][2] = rs_1.getString("借书时间");
                        }

                        while(i < rowData.length) {
                            rowData[i][0] = null;
                            rowData[i][1] = null;
                            rowData[i][2] = null;
                            ++i;
                        }

                        TableModel tml = new DefaultTableModel(rowData, Name);
                        table.setModel(tml);
                        dbConn.close();
                        lblNewLabel_1.setVisible(true);
                    } catch (SQLException var34) {
                        var34.printStackTrace();
                        lblNewLabel_2.setVisible(true);
                    } catch (ClassNotFoundException var35) {
                        var35.printStackTrace();
                        lblNewLabel_2.setVisible(true);
                    }

                    if (lblNewLabel_2.isVisible() || lblNewLabel_3.isVisible()) {
                        lblNewLabel_1.setVisible(false);
                    }

                    if (lblNewLabel_1.isVisible()) {
                        lblNewLabel_2.setVisible(false);
                        lblNewLabel_3.setVisible(false);
                    }
                }

            }
        });
        btnNewButton.setBounds(10, 356, 93, 23);
        this.contentPane.add(btnNewButton);
    }
}
