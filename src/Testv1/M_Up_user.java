//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//
//修改借书者信息
package Testv1;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class M_Up_user extends JFrame {
    private JPanel contentPane;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    private JTextField textField_4;
    private JTextField textField_5;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    M_Up_user frame = new M_Up_user();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public M_Up_user() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 450, 300);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JLabel lblNewLabel = new JLabel("想要修改/添加的借书者的账号是");
        lblNewLabel.setBounds(10, 28, 187, 15);
        this.contentPane.add(lblNewLabel);
        this.textField = new JTextField();
        this.textField.setBounds(195, 25, 104, 21);
        this.contentPane.add(this.textField);
        this.textField.setColumns(10);
        JLabel lblNewLabel_1 = new JLabel("姓名");
        lblNewLabel_1.setBounds(10, 64, 54, 15);
        this.contentPane.add(lblNewLabel_1);
        this.textField_1 = new JTextField();
        this.textField_1.setBounds(49, 61, 90, 21);
        this.contentPane.add(this.textField_1);
        this.textField_1.setColumns(10);
        JLabel lblNewLabel_2 = new JLabel("性别");
        lblNewLabel_2.setBounds(10, 104, 54, 15);
        this.contentPane.add(lblNewLabel_2);
        this.textField_2 = new JTextField();
        this.textField_2.setBounds(49, 101, 66, 21);
        this.contentPane.add(this.textField_2);
        this.textField_2.setColumns(10);
        JLabel lblNewLabel_3 = new JLabel("联系电话");
        lblNewLabel_3.setBounds(10, 135, 54, 15);
        this.contentPane.add(lblNewLabel_3);
        this.textField_3 = new JTextField();
        this.textField_3.setBounds(71, 132, 126, 21);
        this.contentPane.add(this.textField_3);
        this.textField_3.setColumns(10);
        JLabel lblNewLabel_4 = new JLabel("职业");
        lblNewLabel_4.setBounds(10, 166, 54, 15);
        this.contentPane.add(lblNewLabel_4);
        this.textField_4 = new JTextField();
        this.textField_4.setBounds(49, 163, 148, 21);
        this.contentPane.add(this.textField_4);
        this.textField_4.setColumns(10);
        JLabel lblNewLabel_5 = new JLabel("所在单位");
        lblNewLabel_5.setBounds(0, 197, 54, 15);
        this.contentPane.add(lblNewLabel_5);
        this.textField_5 = new JTextField();
        this.textField_5.setBounds(59, 194, 195, 21);
        this.contentPane.add(this.textField_5);
        this.textField_5.setColumns(10);
        final JLabel lblNewLabel_6 = new JLabel("操作成功");
        lblNewLabel_6.setBounds(113, 232, 54, 15);
        this.contentPane.add(lblNewLabel_6);
        lblNewLabel_6.setVisible(false);
        final JLabel lblNewLabel_8 = new JLabel("请输入“男”或“女”");
        lblNewLabel_8.setForeground(Color.RED);
        lblNewLabel_8.setBounds(158, 104, 148, 15);
        this.contentPane.add(lblNewLabel_8);
        lblNewLabel_8.setVisible(false);
        final JLabel lblNewLabel_7 = new JLabel("必填");
        lblNewLabel_7.setForeground(Color.RED);
        lblNewLabel_7.setBounds(309, 28, 66, 15);
        this.contentPane.add(lblNewLabel_7);
        lblNewLabel_7.setVisible(false);
        final JLabel lblNewLabel_9 = new JLabel("数据库中没有该账户");
        lblNewLabel_9.setForeground(Color.RED);
        lblNewLabel_9.setBounds(200, 52, 135, 15);
        this.contentPane.add(lblNewLabel_9);
        lblNewLabel_9.setVisible(false);
        final JLabel lblNewLabel_10 = new JLabel("每个项目都必填");
        lblNewLabel_10.setForeground(Color.RED);
        lblNewLabel_10.setBounds(273, 135, 116, 15);
        this.contentPane.add(lblNewLabel_10);
        lblNewLabel_10.setVisible(false);
        JButton btnNewButton = new JButton("返回");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                M_Up_user.this.setVisible(false);
            }
        });
        btnNewButton.setBounds(331, 228, 93, 23);
        this.contentPane.add(btnNewButton);
        JButton btnNewButton_1 = new JButton("修改");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = new String(M_Up_user.this.textField_1.getText());
                String work = new String(M_Up_user.this.textField_4.getText());
                String sex = new String(M_Up_user.this.textField_2.getText());
                String phone = new String(M_Up_user.this.textField_3.getText());
                String site = new String(M_Up_user.this.textField_5.getText());
                String num = new String(M_Up_user.this.textField.getText());
                lblNewLabel_10.setVisible(false);
                if (num.equals("")) {
                    lblNewLabel_7.setVisible(true);
                } else {
                    lblNewLabel_7.setVisible(false);
                }

                String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                String userName = "root";
                String userPwd = "ys124126";
                Connection dbConn = null;

                try {
                    Statement state = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    state = dbConn.createStatement();
                    PreparedStatement pst = null;
                    String sql;
                    if (!name.equals("")) {
                        sql = "update 借书者 set 姓名=? where 借书证号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, name);
                        pst.setString(2, num);
                        pst.executeUpdate();
                    }

                    if (!sex.equals("")) {
                        try {
                            sql = "update 借书者 set 性别=? where 借书证号=?";
                            pst = dbConn.prepareStatement(sql);
                            pst.setString(1, sex);
                            pst.setString(2, num);
                            pst.executeUpdate();
                            lblNewLabel_8.setVisible(false);
                        } catch (Exception var15) {
                            var15.printStackTrace();
                            lblNewLabel_8.setVisible(true);
                            lblNewLabel_6.setVisible(false);
                        }
                    }

                    if (!work.equals("")) {
                        sql = "update 借书者 set 职业=? where 借书证号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, work);
                        pst.setString(2, num);
                        pst.executeUpdate();
                    }

                    if (!phone.equals("")) {
                        sql = "update 借书者 set 联系电话=? where 借书证号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, phone);
                        pst.setString(2, num);
                        pst.executeUpdate();
                    }

                    if (!site.equals("")) {
                        sql = "update 借书者 set 所在单位=? where 借书证号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, site);
                        pst.setString(2, num);
                        pst.executeUpdate();
                    }

                    pst.close();
                    dbConn.close();
                    lblNewLabel_9.setVisible(false);
                    lblNewLabel_6.setVisible(true);
                } catch (SQLException var16) {
                    var16.printStackTrace();
                } catch (ClassNotFoundException var17) {
                    var17.printStackTrace();
                }

                if (lblNewLabel_8.isVisible() || lblNewLabel_9.isVisible()) {
                    lblNewLabel_6.setVisible(false);
                }

                if (lblNewLabel_6.isVisible()) {
                    lblNewLabel_7.setVisible(false);
                    lblNewLabel_8.setVisible(false);
                    lblNewLabel_9.setVisible(false);
                }

            }
        });
        btnNewButton_1.setBounds(10, 228, 93, 23);
        this.contentPane.add(btnNewButton_1);
        JButton btnNewButton_2 = new JButton("添加");
        btnNewButton_2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = new String(M_Up_user.this.textField_1.getText());
                String work = new String(M_Up_user.this.textField_4.getText());
                String sex = new String(M_Up_user.this.textField_2.getText());
                String phone = new String(M_Up_user.this.textField_3.getText());
                String site = new String(M_Up_user.this.textField_5.getText());
                String num = new String(M_Up_user.this.textField.getText());
                if (!num.equals("") && !name.equals("") && !sex.equals("") && !phone.equals("") && !work.equals("") && !site.equals("")) {
                    lblNewLabel_10.setVisible(false);
                    String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                    String userName = "root";
                    String userPwd = "ys124126";
                    Connection dbConn = null;

                    try {
                        Statement state = null;
                        Class.forName("com.mysql.cj.jdbc.Driver");
                        dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                        state = dbConn.createStatement();
                        PreparedStatement pst = null;
                        String sql = "insert into 借书者 values(?,?,?,?,?,?,?)";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, num);
                        pst.setString(2, name);
                        pst.setString(3, sex);
                        pst.setString(4, phone);
                        pst.setString(5, work);
                        pst.setString(6, site);
                        pst.setString(7, "123456");
                        pst.addBatch();
                        pst.executeBatch();
                        lblNewLabel_8.setVisible(false);
                        lblNewLabel_6.setVisible(true);
                    } catch (SQLException var15) {
                        var15.printStackTrace();
                        lblNewLabel_8.setVisible(true);
                    } catch (ClassNotFoundException var16) {
                        var16.printStackTrace();
                        lblNewLabel_8.setVisible(true);
                    }
                } else {
                    lblNewLabel_10.setVisible(true);
                    lblNewLabel_6.setVisible(false);
                }

                if (lblNewLabel_8.isVisible() || lblNewLabel_9.isVisible()) {
                    lblNewLabel_6.setVisible(false);
                }

                if (lblNewLabel_6.isVisible()) {
                    lblNewLabel_7.setVisible(false);
                    lblNewLabel_8.setVisible(false);
                    lblNewLabel_9.setVisible(false);
                    lblNewLabel_10.setVisible(false);
                }

            }
        });
        btnNewButton_2.setBounds(177, 225, 93, 23);
        this.contentPane.add(btnNewButton_2);
    }
}
