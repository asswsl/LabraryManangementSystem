//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//
//借书
package Testv1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class Lend extends JFrame {
    private JPanel contentPane;
    private JTextField textField;
    private JTextField textField_1;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Lend frame = new Lend();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public Lend() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 633, 414);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JButton btnNewButton_1 = new JButton("返回");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Lend.this.setVisible(false);
            }
        });
        btnNewButton_1.setBounds(514, 342, 93, 23);
        this.contentPane.add(btnNewButton_1);
        JLabel lblNewLabel_1 = new JLabel("输入书号");
        lblNewLabel_1.setBounds(10, 315, 54, 15);
        this.contentPane.add(lblNewLabel_1);
        this.textField_1 = new JTextField();
        this.textField_1.setBounds(63, 312, 93, 21);
        this.contentPane.add(this.textField_1);
        this.textField_1.setColumns(10);
        final JLabel lblNewLabel_2 = new JLabel("操作成功");
        lblNewLabel_2.setBounds(163, 346, 54, 15);
        this.contentPane.add(lblNewLabel_2);
        lblNewLabel_2.setVisible(false);
        final JLabel lblNewLabel = new JLabel("操作失败");
        lblNewLabel.setForeground(Color.RED);
        lblNewLabel.setBounds(113, 346, 54, 15);
        this.contentPane.add(lblNewLabel);
        lblNewLabel.setVisible(false);
        JPanel panel = new JPanel();
        panel.setBounds(10, 10, 597, 295);
        this.contentPane.add(panel);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 46, 300, 150);
        panel.add(scrollPane);
        final String[] Name = new String[]{"书号", "书名", "作者", "出版社", "定价", "内容简介", "状态", "分类"};
        final Object[][] rowData = new Object[100][8];
        final JTable table = new JTable(rowData, Name);
        table.setBounds(297, 179, -279, -124);
        table.setRowHeight(30);
        table.getColumnModel().getColumn(0).setPreferredWidth(110);
        table.setPreferredScrollableViewportSize(new Dimension(580, 300));
        scrollPane.setViewportView(table);

        try {
            String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
            String userName = "root";
            String userPwd = "ys124126";
            Connection dbConn = null;
            Statement state = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
            state = dbConn.createStatement();
            String sql0 = "select * from 图书";
            ResultSet rs = state.executeQuery(sql0);

            for(int i = 0; rs.next() && i < rowData.length; ++i) {
                rowData[i][0] = rs.getString(1);
                rowData[i][1] = rs.getString(2);
                rowData[i][2] = rs.getString(3);
                rowData[i][3] = rs.getString(4);
                rowData[i][4] = rs.getString(5);
                rowData[i][5] = rs.getString(6);
                rowData[i][6] = rs.getString(7);
                rowData[i][7] = rs.getString(8);
            }

            dbConn.close();
        } catch (SQLException var18) {
            var18.printStackTrace();
        } catch (ClassNotFoundException var19) {
            var19.printStackTrace();
        }

        final JLabel lblNewLabel_3 = new JLabel("一次最多借8本");
        lblNewLabel_3.setForeground(Color.RED);
        lblNewLabel_3.setBounds(221, 315, 93, 15);
        this.contentPane.add(lblNewLabel_3);
        lblNewLabel_3.setVisible(false);
        JButton btnNewButton = new JButton("查询");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BookFind bf = new BookFind();
                bf.setVisible(true);
            }
        });
        btnNewButton.setBounds(514, 311, 93, 23);
        this.contentPane.add(btnNewButton);
        JButton btnNewButton_2 = new JButton("借阅");
        btnNewButton_2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String textBook = new String(Lend.this.textField_1.getText());
                String num = new String();
                new String();
                new String();
                String tmp = null;
                String status = null;

                String dbURL;
                String userName;
                String userPwd;
                Connection dbConn;
                Statement state;
                try {
                     dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                     userName = "root";
                     userPwd = "ys124126";
                    dbConn = null;
                    state = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    state = dbConn.createStatement();
                    String sql0x = "SELECT 操作账号,时间 FROM 操作账号 ORDER BY 时间 DESC limit 1";
                    ResultSet rsx = state.executeQuery(sql0x);

                    String s1;
                    for(s1 = new String(); rsx.next(); s1 = rsx.getString(1)) {
                    }

                    num = s1;
                    Statement sta = dbConn.createStatement();
                    String sql = "select count(*) from 借书 where 借书证号='" + s1 + "'";

                    for(ResultSet re = sta.executeQuery(sql); re.next(); tmp = re.getString(1)) {
                    }
                } catch (SQLException var34) {
                    var34.printStackTrace();
                } catch (ClassNotFoundException var35) {
                    var35.printStackTrace();
                }

                if (tmp.equals("8")) {
                    lblNewLabel_3.setVisible(true);
                    lblNewLabel_2.setVisible(false);
                } else {
                    try {
                         dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                         userName = "root";
                         userPwd = "ys124126";
                        dbConn = null;
                        state = null;
                        Class.forName("com.mysql.cj.jdbc.Driver");
                        dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                        state = dbConn.createStatement();
                        Statement state_1 = dbConn.createStatement();
                        String sql1 = "select * from 借书者 where 借书证号='" + num + "'";
                        ResultSet rs1 = state_1.executeQuery(sql1);

                        String s2;
                        for(s2 = new String(); rs1.next(); s2 = rs1.getString(2)) {
                        }

                        Statement state_2 = dbConn.createStatement();
                        String sql2 = "select * from 图书 where 书号='" + textBook + "'";
                        ResultSet rs2 = state_2.executeQuery(sql2);

                        String s3;
                        for(s3 = new String(); rs2.next(); s3 = rs2.getString(2)) {
                        }

                        Statement state_4 = dbConn.createStatement();
                        String sql3 = "select * from 图书 where 书号='" + textBook + "'";
                        ResultSet rs3 = state_4.executeQuery(sql3);

                        String s4;
                        for(s4 = new String(); rs3.next(); s4 = rs3.getString("状态")) {
                        }

                        if (s4.equals("已借出")) {
                            lblNewLabel.setVisible(true);
                        } else {
                            lblNewLabel.setVisible(false);
                            Calendar c = Calendar.getInstance();
                            PreparedStatement pst = null;
                            pst = dbConn.prepareStatement("insert into 借书 values(?,?,?,?,?)");
                            pst.setString(1, num);
                            pst.setString(2, s2);
                            pst.setString(3, textBook);
                            pst.setString(4, s3);
                            pst.setTimestamp(5, new Timestamp(c.getTimeInMillis()));
                            pst.addBatch();
                            pst.executeBatch();
                            PreparedStatement pst1 = null;
                            pst1 = dbConn.prepareStatement("update 图书 set 状态=? where 书号=?");
                            pst1.setString(1, "已借出");
                            pst1.setString(2, textBook);
                            pst1.addBatch();
                            pst1.executeBatch();
                            lblNewLabel_2.setVisible(true);
                            String sql0 = "select * from 图书";
                            ResultSet rs = state.executeQuery(sql0);

                            for(int i = 0; rs.next() && i < rowData.length; ++i) {
                                rowData[i][0] = rs.getString(1);
                                rowData[i][1] = rs.getString(2);
                                rowData[i][2] = rs.getString(3);
                                rowData[i][3] = rs.getString(4);
                                rowData[i][4] = rs.getString(5);
                                rowData[i][5] = rs.getString(6);
                                rowData[i][6] = rs.getString(7);
                                rowData[i][7] = rs.getString(8);
                            }

                            TableModel tml = new DefaultTableModel(rowData, Name);
                            table.setModel(tml);
                            dbConn.close();
                        }
                    } catch (SQLException var32) {
                        var32.printStackTrace();
                        lblNewLabel.setVisible(true);
                        lblNewLabel_2.setVisible(false);
                    } catch (ClassNotFoundException var33) {
                        var33.printStackTrace();
                        lblNewLabel.setVisible(true);
                        lblNewLabel_2.setVisible(false);
                    }

                    if (lblNewLabel_2.isVisible()) {
                        lblNewLabel.setVisible(false);
                    }
                }

            }
        });
        btnNewButton_2.setBounds(10, 342, 93, 23);
        this.contentPane.add(btnNewButton_2);
    }
}
