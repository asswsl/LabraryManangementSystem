//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//
//修改图书信息
package Testv1;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class M_Up_book extends JFrame {
    private JPanel contentPane;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    private JTextField textField_4;
    private JTextField textField_5;
    private JTextField textField_6;
    private JTextField textField_7;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    M_Up_book frame = new M_Up_book();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public M_Up_book() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 450, 300);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JLabel lblNewLabel = new JLabel("输入要修改的图书书号");
        lblNewLabel.setBounds(10, 10, 133, 15);
        this.contentPane.add(lblNewLabel);
        this.textField = new JTextField();
        this.textField.setBounds(139, 7, 106, 21);
        this.contentPane.add(this.textField);
        this.textField.setColumns(10);
        JLabel lblNewLabel_1 = new JLabel("书名");
        lblNewLabel_1.setBounds(10, 35, 54, 15);
        this.contentPane.add(lblNewLabel_1);
        this.textField_1 = new JTextField();
        this.textField_1.setBounds(51, 32, 227, 21);
        this.contentPane.add(this.textField_1);
        this.textField_1.setColumns(10);
        JLabel lblNewLabel_2 = new JLabel("分类");
        lblNewLabel_2.setBounds(10, 236, 54, 15);
        this.contentPane.add(lblNewLabel_2);
        JLabel lblNewLabel_3 = new JLabel("作者");
        lblNewLabel_3.setBounds(10, 69, 54, 15);
        this.contentPane.add(lblNewLabel_3);
        this.textField_2 = new JTextField();
        this.textField_2.setBounds(51, 66, 227, 21);
        this.contentPane.add(this.textField_2);
        this.textField_2.setColumns(10);
        JLabel lblNewLabel_4 = new JLabel("出版社");
        lblNewLabel_4.setBounds(10, 97, 54, 15);
        this.contentPane.add(lblNewLabel_4);
        this.textField_3 = new JTextField();
        this.textField_3.setBounds(51, 94, 227, 21);
        this.contentPane.add(this.textField_3);
        this.textField_3.setColumns(10);
        JLabel lblNewLabel_5 = new JLabel("定价");
        lblNewLabel_5.setBounds(10, 132, 54, 15);
        this.contentPane.add(lblNewLabel_5);
        this.textField_4 = new JTextField();
        this.textField_4.setBounds(51, 129, 92, 21);
        this.contentPane.add(this.textField_4);
        this.textField_4.setColumns(10);
        JLabel lblNewLabel_6 = new JLabel("内容简介");
        lblNewLabel_6.setBounds(10, 160, 54, 15);
        this.contentPane.add(lblNewLabel_6);
        this.textField_5 = new JTextField();
        this.textField_5.setBounds(74, 157, 339, 43);
        this.contentPane.add(this.textField_5);
        this.textField_5.setColumns(10);
        JLabel lblNewLabel_7 = new JLabel("状态");
        lblNewLabel_7.setBounds(10, 211, 54, 15);
        this.contentPane.add(lblNewLabel_7);
        this.textField_6 = new JTextField();
        this.textField_6.setBounds(51, 208, 81, 21);
        this.contentPane.add(this.textField_6);
        this.textField_6.setColumns(10);
        this.textField_7 = new JTextField();
        this.textField_7.setBounds(51, 233, 106, 21);
        this.contentPane.add(this.textField_7);
        this.textField_7.setColumns(10);
        final JLabel lblNewLabel_8 = new JLabel("修改成功");
        lblNewLabel_8.setForeground(Color.BLACK);
        lblNewLabel_8.setBounds(198, 211, 54, 15);
        this.contentPane.add(lblNewLabel_8);
        lblNewLabel_8.setVisible(false);
        final JLabel lblNewLabel_9 = new JLabel("必填");
        lblNewLabel_9.setForeground(Color.RED);
        lblNewLabel_9.setBounds(255, 10, 54, 15);
        this.contentPane.add(lblNewLabel_9);
        lblNewLabel_9.setVisible(false);
        final JLabel lblNewLabel_10 = new JLabel("数据库中没有该图书");
        lblNewLabel_10.setForeground(Color.RED);
        lblNewLabel_10.setBounds(297, 10, 116, 15);
        this.contentPane.add(lblNewLabel_10);
        lblNewLabel_10.setVisible(false);
        JButton btnNewButton = new JButton("确认");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String num = new String(M_Up_book.this.textField.getText());
                String name = new String(M_Up_book.this.textField_1.getText());
                String writer = new String(M_Up_book.this.textField_2.getText());
                String publish = new String(M_Up_book.this.textField_3.getText());
                String price = new String(M_Up_book.this.textField_4.getText());
                String intro = new String(M_Up_book.this.textField_5.getText());
                String status = new String(M_Up_book.this.textField_6.getText());
                String cla = new String(M_Up_book.this.textField_7.getText());
                if (num.equals("")) {
                    lblNewLabel_9.setVisible(true);
                } else {
                    lblNewLabel_9.setVisible(false);
                }

                String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                String userName = "root";
                String userPwd = "ys124126";
                Connection dbConn = null;

                try {
                    Statement state = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    state = dbConn.createStatement();
                    PreparedStatement pst = null;
                    String sql;
                    if (!name.equals("")) {
                        sql = "update 图书 set 书名=? where 书号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, name);
                        pst.setString(2, num);
                        pst.executeUpdate();
                    }

                    if (!writer.equals("")) {
                        sql = "update 图书 set 书名=? where 书号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, writer);
                        pst.setString(2, num);
                        pst.executeUpdate();
                    }

                    if (!publish.equals("")) {
                        sql = "update 图书 set 出版社=? where 书号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, publish);
                        pst.setString(2, num);
                        pst.executeUpdate();
                    }

                    if (!price.equals("")) {
                        sql = "update 图书 set 定价=? where 书号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, price);
                        pst.setString(2, num);
                        pst.executeUpdate();
                    }

                    if (!intro.equals("")) {
                        sql = "update 图书 set 内容简介=? where 书号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, intro);
                        pst.setString(2, num);
                        pst.executeUpdate();
                    }

                    if (!status.equals("")) {
                        sql = "update 图书 set 状态=? where 书号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, status);
                        pst.setString(2, num);
                        pst.executeUpdate();
                    }

                    if (!cla.equals("")) {
                        sql = "update 图书 set 分类=? where 书号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, cla);
                        pst.setString(2, num);
                        pst.executeUpdate();
                    }

                    pst.close();
                    dbConn.close();
                    lblNewLabel_10.setVisible(false);
                } catch (SQLException var17) {
                    var17.printStackTrace();
                } catch (ClassNotFoundException var18) {
                    var18.printStackTrace();
                } catch (Exception var19) {
                    var19.printStackTrace();
                    lblNewLabel_10.setVisible(true);
                }

                if (!lblNewLabel_9.isVisible() && !lblNewLabel_10.isVisible()) {
                    lblNewLabel_8.setVisible(true);
                } else {
                    lblNewLabel_8.setVisible(false);
                }

            }
        });
        btnNewButton.setBounds(198, 232, 93, 23);
        this.contentPane.add(btnNewButton);
        JButton btnNewButton_1 = new JButton("返回");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                M_Up_book.this.setVisible(false);
            }
        });
        btnNewButton_1.setBounds(331, 232, 93, 23);
        this.contentPane.add(btnNewButton_1);
    }
}
