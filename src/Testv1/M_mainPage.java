//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//
//主页面
package Testv1;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class M_mainPage extends JFrame {
    private JPanel contentPane;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    private JTextField textField_4;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    M_mainPage frame = new M_mainPage();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public M_mainPage() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 452, 338);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JButton btnNewButton = new JButton("图书查询");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BookFind bf = new BookFind();
                bf.setVisible(true);
            }
        });
        btnNewButton.setBounds(0, 10, 93, 23);
        this.contentPane.add(btnNewButton);
        JButton btnNewButton_1 = new JButton("图书添加");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AddBook Ad = new AddBook();
                Ad.setVisible(true);
            }
        });
        btnNewButton_1.setBounds(0, 71, 93, 23);
        this.contentPane.add(btnNewButton_1);
        JButton btnNewButton_2 = new JButton("图书编辑");
        btnNewButton_2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UpdateBook Ub = new UpdateBook();
                Ub.setVisible(true);
            }
        });
        btnNewButton_2.setBounds(0, 129, 93, 23);
        this.contentPane.add(btnNewButton_2);
        JButton btnNewButton_4 = new JButton("返回");
        btnNewButton_4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                M_mainPage.this.setVisible(false);
            }
        });
        btnNewButton_4.setBounds(320, 227, 93, 23);
        this.contentPane.add(btnNewButton_4);
        JButton btnNewButton_5 = new JButton("读者查询与编辑");
        btnNewButton_5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UpdateUser Uuser = new UpdateUser();
                Uuser.setVisible(true);
            }
        });
        btnNewButton_5.setBounds(0, 227, 127, 23);
        this.contentPane.add(btnNewButton_5);
        JButton btnNewButton_6 = new JButton("修改个人信息");
        btnNewButton_6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                M_upPerson Mpage = new M_upPerson();
                Mpage.setVisible(true);
            }
        });
        btnNewButton_6.setBounds(316, 47, 111, 23);
        this.contentPane.add(btnNewButton_6);
        JLabel lblNewLabel = new JLabel("欢迎使用");
        lblNewLabel.setFont(new Font("宋体", 0, 25));
        lblNewLabel.setBounds(167, 71, 127, 51);
        this.contentPane.add(lblNewLabel);
        JButton btnNewButton_7 = new JButton("修改密码");
        btnNewButton_7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UpdatePassword Upw = new UpdatePassword();
                Upw.setVisible(true);
            }
        });
        btnNewButton_7.setBounds(320, 118, 93, 23);
        this.contentPane.add(btnNewButton_7);
        JButton btnNewButton_3 = new JButton("借书管理");
        btnNewButton_3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                lend_manage lm = new lend_manage();
                lm.setVisible(true);
            }
        });
        btnNewButton_3.setBounds(0, 178, 93, 23);
        this.contentPane.add(btnNewButton_3);
    }
}
