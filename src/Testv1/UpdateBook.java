//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Testv1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class UpdateBook extends JFrame {
    private JPanel contentPane;
    private JTextField textField;
    private JTextField textField_1;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UpdateBook frame = new UpdateBook();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public UpdateBook() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 612, 458);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JLabel lblNewLabel = new JLabel("编辑图书");
        lblNewLabel.setFont(new Font("宋体", 0, 20));
        lblNewLabel.setBounds(255, 5, 142, 35);
        this.contentPane.add(lblNewLabel);
        JLabel lblNewLabel_1 = new JLabel("输入书号");
        lblNewLabel_1.setBounds(20, 54, 54, 15);
        this.contentPane.add(lblNewLabel_1);
        this.textField = new JTextField();
        this.textField.setBounds(84, 51, 131, 21);
        this.contentPane.add(this.textField);
        this.textField.setColumns(10);
        JLabel lblNewLabel_2 = new JLabel("");
        lblNewLabel_2.setBounds(10, 128, 54, 15);
        this.contentPane.add(lblNewLabel_2);
        JButton btnNewButton_1 = new JButton("返回");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UpdateBook.this.setVisible(false);
            }
        });
        btnNewButton_1.setBounds(493, 386, 93, 23);
        this.contentPane.add(btnNewButton_1);
        JPanel panel = new JPanel();
        panel.setBounds(10, 86, 576, 262);
        this.contentPane.add(panel);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 46, 300, 150);
        panel.add(scrollPane);
        final String[] Name = new String[]{"书号", "书名", "作者", "出版社", "定价", "内容简介", "状态", "分类"};
        final Object[][] rowData = new Object[100][8];
        final JTable table = new JTable(rowData, Name);
        table.setBounds(297, 179, -279, -124);
        table.setRowHeight(30);
        table.getColumnModel().getColumn(0).setPreferredWidth(110);
        table.setPreferredScrollableViewportSize(new Dimension(550, 300));
        scrollPane.setViewportView(table);

        try {
            String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
            String userName = "root";
            String userPwd = "ys124126";
            Connection dbConn = null;
            Statement state = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
            state = dbConn.createStatement();
            String sql0 = "select * from 图书";
            ResultSet rs = state.executeQuery(sql0);

            for(int i = 0; rs.next() && i < rowData.length; ++i) {
                rowData[i][0] = rs.getString(1);
                rowData[i][1] = rs.getString(2);
                rowData[i][2] = rs.getString(3);
                rowData[i][3] = rs.getString(4);
                rowData[i][4] = rs.getString(5);
                rowData[i][5] = rs.getString(6);
                rowData[i][6] = rs.getString(7);
                rowData[i][7] = rs.getString(8);
            }

            dbConn.close();
        } catch (SQLException var18) {
            var18.printStackTrace();
        } catch (ClassNotFoundException var19) {
            var19.printStackTrace();
        }

        JButton btnNewButton = new JButton("查询");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str0 = new String(UpdateBook.this.textField.getText());

                try {
                    String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                    String userName = "root";
                    String userPwd = "ys124126";
                    Connection dbConn = null;
                    Statement state = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    state = dbConn.createStatement();
                    String sql0 = "select * from 图书 where 书号='" + str0 + "'";
                    ResultSet rs = state.executeQuery(sql0);

                    int i;
                    for(i = 0; rs.next() && i < rowData.length; ++i) {
                        rowData[i][0] = rs.getString(1);
                        rowData[i][1] = rs.getString(2);
                        rowData[i][2] = rs.getString(3);
                        rowData[i][3] = rs.getString(4);
                        rowData[i][4] = rs.getString(5);
                        rowData[i][5] = rs.getString(6);
                        rowData[i][6] = rs.getString(7);
                        rowData[i][7] = rs.getString(8);
                    }

                    while(i < rowData.length) {
                        rowData[i][0] = null;
                        rowData[i][1] = null;
                        rowData[i][2] = null;
                        rowData[i][3] = null;
                        rowData[i][4] = null;
                        rowData[i][5] = null;
                        rowData[i][6] = null;
                        rowData[i][7] = null;
                        ++i;
                    }

                    TableModel tml = new DefaultTableModel(rowData, Name);
                    table.setModel(tml);
                    dbConn.close();
                } catch (SQLException var12) {
                    var12.printStackTrace();
                } catch (ClassNotFoundException var13) {
                    var13.printStackTrace();
                }

            }
        });
        btnNewButton.setBounds(248, 50, 93, 23);
        this.contentPane.add(btnNewButton);
        JButton btnNewButton_2 = new JButton("修改");
        btnNewButton_2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                M_Up_book Upbook = new M_Up_book();
                Upbook.setVisible(true);
            }
        });
        btnNewButton_2.setBounds(20, 358, 93, 23);
        this.contentPane.add(btnNewButton_2);
        JButton btnNewButton_3 = new JButton("复位");
        btnNewButton_3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                    String userName = "root";
                    String userPwd = "ys124126";
                    Connection dbConn = null;
                    Statement state = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    state = dbConn.createStatement();
                    String sql0 = "select * from 图书";
                    ResultSet rs = state.executeQuery(sql0);

                    int i;
                    for(i = 0; rs.next() && i < rowData.length; ++i) {
                        rowData[i][0] = rs.getString(1);
                        rowData[i][1] = rs.getString(2);
                        rowData[i][2] = rs.getString(3);
                        rowData[i][3] = rs.getString(4);
                        rowData[i][4] = rs.getString(5);
                        rowData[i][5] = rs.getString(6);
                        rowData[i][6] = rs.getString(7);
                        rowData[i][7] = rs.getString(8);
                    }

                    while(i < rowData.length) {
                        rowData[i][0] = null;
                        rowData[i][1] = null;
                        rowData[i][2] = null;
                        rowData[i][3] = null;
                        rowData[i][4] = null;
                        rowData[i][5] = null;
                        rowData[i][6] = null;
                        rowData[i][7] = null;
                        ++i;
                    }

                    TableModel tml = new DefaultTableModel(rowData, Name);
                    table.setModel(tml);
                    dbConn.close();
                } catch (SQLException var11) {
                    var11.printStackTrace();
                } catch (ClassNotFoundException var12) {
                    var12.printStackTrace();
                }

            }
        });
        btnNewButton_3.setBounds(449, 50, 93, 23);
        this.contentPane.add(btnNewButton_3);
        JLabel lblNewLabel_3 = new JLabel("要删除的图书书号");
        lblNewLabel_3.setBounds(255, 362, 100, 15);
        this.contentPane.add(lblNewLabel_3);
        this.textField_1 = new JTextField();
        this.textField_1.setBounds(356, 359, 80, 21);
        this.contentPane.add(this.textField_1);
        this.textField_1.setColumns(10);
        final JLabel lblNewLabel_4 = new JLabel("操作成功");
        lblNewLabel_4.setBounds(248, 390, 54, 15);
        this.contentPane.add(lblNewLabel_4);
        lblNewLabel_4.setVisible(false);
        final JLabel lblNewLabel_5 = new JLabel("操作失败");
        lblNewLabel_5.setForeground(Color.RED);
        lblNewLabel_5.setBounds(161, 390, 54, 15);
        this.contentPane.add(lblNewLabel_5);
        lblNewLabel_5.setVisible(false);
        JButton btnNewButton_4 = new JButton("删除");
        btnNewButton_4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String num = new String(UpdateBook.this.textField_1.getText());

                try {
                    String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                    String userName = "root";
                    String userPwd = "ys124126";
                    Connection dbConn = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    String sql0 = "delete from 图书 where 书号= ?";
                    PreparedStatement pst = dbConn.prepareStatement(sql0);
                    pst.setString(1, num);
                    pst.addBatch();
                    pst.executeBatch();
                    Statement state = dbConn.createStatement();
                    String sql = "select * from 图书";
                    ResultSet rs = state.executeQuery(sql);

                    int i;
                    for(i = 0; rs.next() && i < rowData.length; ++i) {
                        rowData[i][0] = rs.getString(1);
                        rowData[i][1] = rs.getString(2);
                        rowData[i][2] = rs.getString(3);
                        rowData[i][3] = rs.getString(4);
                        rowData[i][4] = rs.getString(5);
                        rowData[i][5] = rs.getString(6);
                        rowData[i][6] = rs.getString(7);
                        rowData[i][7] = rs.getString(8);
                    }

                    while(i < rowData.length) {
                        rowData[i][0] = null;
                        rowData[i][1] = null;
                        rowData[i][2] = null;
                        rowData[i][3] = null;
                        rowData[i][4] = null;
                        rowData[i][5] = null;
                        rowData[i][6] = null;
                        rowData[i][7] = null;
                        ++i;
                    }

                    TableModel tml = new DefaultTableModel(rowData, Name);
                    table.setModel(tml);
                    dbConn.close();
                } catch (SQLException var14) {
                    var14.printStackTrace();
                    lblNewLabel_5.setVisible(true);
                } catch (ClassNotFoundException var15) {
                    var15.printStackTrace();
                    lblNewLabel_5.setVisible(true);
                }

                if (lblNewLabel_4.isVisible()) {
                    lblNewLabel_5.setVisible(false);
                }

                if (lblNewLabel_5.isVisible()) {
                    lblNewLabel_4.setVisible(false);
                }

            }
        });
        btnNewButton_4.setBounds(150, 358, 93, 23);
        this.contentPane.add(btnNewButton_4);
    }
}
