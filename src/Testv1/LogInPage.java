//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Testv1;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class LogInPage extends JFrame {
    private JPanel contentPane;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    LogInPage frame = new LogInPage();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public LogInPage() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 450, 300);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JLabel lblNewLabel = new JLabel("图书管理系统");
        lblNewLabel.setBounds(130, 10, 174, 34);
        lblNewLabel.setFont(new Font("宋体", 0, 29));
        this.contentPane.add(lblNewLabel);
        JButton btnNewButton = new JButton("管理员登录");
        btnNewButton.setFont(new Font("宋体", 0, 17));
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Manager_Login MPage = new Manager_Login();
                MPage.setVisible(true);
            }
        });
        btnNewButton.setBounds(146, 92, 144, 39);
        this.contentPane.add(btnNewButton);
        JButton btnNewButton_1 = new JButton("读者登录");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                User_Login UPage = new User_Login();
                UPage.setVisible(true);
            }
        });
        btnNewButton_1.setFont(new Font("宋体", 0, 17));
        btnNewButton_1.setBounds(146, 159, 144, 39);
        this.contentPane.add(btnNewButton_1);
    }
}
