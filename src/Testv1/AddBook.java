//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//
//添加图书
package Testv1;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class AddBook extends JFrame {
    private JPanel contentPane;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    private JTextField textField_4;
    private JTextField textField_5;
    private JTextField textField_6;
    private JTextField textField_7;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    AddBook frame = new AddBook();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public AddBook() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 450, 360);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JLabel lblNewLabel = new JLabel("添加图书");
        lblNewLabel.setFont(new Font("宋体", 0, 20));
        lblNewLabel.setBounds(170, 10, 92, 38);
        this.contentPane.add(lblNewLabel);
        JLabel lblNewLabel_1 = new JLabel("书号");
        lblNewLabel_1.setBounds(32, 48, 54, 15);
        this.contentPane.add(lblNewLabel_1);
        this.textField = new JTextField();
        this.textField.setBounds(71, 45, 148, 21);
        this.contentPane.add(this.textField);
        this.textField.setColumns(10);
        JLabel lblNewLabel_2 = new JLabel("书名");
        lblNewLabel_2.setBounds(32, 80, 54, 15);
        this.contentPane.add(lblNewLabel_2);
        this.textField_1 = new JTextField();
        this.textField_1.setBounds(71, 77, 242, 21);
        this.contentPane.add(this.textField_1);
        this.textField_1.setColumns(10);
        JLabel lblNewLabel_3 = new JLabel("作者");
        lblNewLabel_3.setBounds(32, 113, 54, 15);
        this.contentPane.add(lblNewLabel_3);
        this.textField_2 = new JTextField();
        this.textField_2.setBounds(71, 110, 242, 21);
        this.contentPane.add(this.textField_2);
        this.textField_2.setColumns(10);
        JLabel lblNewLabel_4 = new JLabel("出版社");
        lblNewLabel_4.setBounds(32, 144, 54, 15);
        this.contentPane.add(lblNewLabel_4);
        JLabel lblNewLabel_5 = new JLabel("定价");
        lblNewLabel_5.setBounds(32, 172, 54, 15);
        this.contentPane.add(lblNewLabel_5);
        this.textField_3 = new JTextField();
        this.textField_3.setBounds(71, 141, 242, 21);
        this.contentPane.add(this.textField_3);
        this.textField_3.setColumns(10);
        this.textField_4 = new JTextField();
        this.textField_4.setBounds(71, 169, 242, 21);
        this.contentPane.add(this.textField_4);
        this.textField_4.setColumns(10);
        JLabel lblNewLabel_6 = new JLabel("内容简介");
        lblNewLabel_6.setBounds(17, 197, 54, 15);
        this.contentPane.add(lblNewLabel_6);
        this.textField_5 = new JTextField();
        this.textField_5.setBounds(71, 194, 242, 21);
        this.contentPane.add(this.textField_5);
        this.textField_5.setColumns(10);
        JLabel lblNewLabel_7 = new JLabel("状态");
        lblNewLabel_7.setBounds(32, 225, 54, 15);
        this.contentPane.add(lblNewLabel_7);
        this.textField_6 = new JTextField();
        this.textField_6.setBounds(71, 222, 242, 21);
        this.contentPane.add(this.textField_6);
        this.textField_6.setColumns(10);
        this.textField_7 = new JTextField();
        this.textField_7.setBounds(71, 250, 242, 21);
        this.contentPane.add(this.textField_7);
        this.textField_7.setColumns(10);
        final JLabel lblNewLabel_9 = new JLabel("操作成功");
        lblNewLabel_9.setBounds(137, 281, 54, 15);
        this.contentPane.add(lblNewLabel_9);
        lblNewLabel_9.setVisible(false);
        final JLabel lblNewLabel_10 = new JLabel("操作失败");
        lblNewLabel_10.setForeground(Color.RED);
        lblNewLabel_10.setBounds(135, 296, 54, 15);
        this.contentPane.add(lblNewLabel_10);
        lblNewLabel_10.setVisible(false);
        JButton btnNewButton = new JButton("确认");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String num = new String(AddBook.this.textField.getText());
                String name = new String(AddBook.this.textField_1.getText());
                String writer = new String(AddBook.this.textField_2.getText());
                String publish = new String(AddBook.this.textField_3.getText());
                String price = new String(AddBook.this.textField_4.getText());
                String intro = new String(AddBook.this.textField_5.getText());
                String status = new String(AddBook.this.textField_6.getText());
                String cla = new String(AddBook.this.textField_7.getText());
                if (!num.equals("") && !name.equals("") && !writer.equals("") && !publish.equals("") && !price.equals("") && !intro.equals("") && !status.equals("") && !cla.equals("")) {
                    String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                    String userName = "root";
                    String userPwd = "ys124126";
                    Connection dbConn = null;

                    try {
                        Class.forName("com.mysql.cj.jdbc.Driver");
                        dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                        PreparedStatement pst = null;
                        pst = dbConn.prepareStatement("insert into 图书 values(?,?,?,?,?,?,?,?)");
                        pst.setString(1, num);
                        pst.setString(2, name);
                        pst.setString(3, writer);
                        pst.setString(4, publish);
                        pst.setString(5, price);
                        pst.setString(6, intro);
                        pst.setString(7, status);
                        pst.setString(8, cla);
                        pst.addBatch();
                        pst.executeBatch();
                        dbConn.close();
                        lblNewLabel_9.setVisible(true);
                    } catch (SQLException var15) {
                        var15.printStackTrace();
                        lblNewLabel_10.setVisible(true);
                    } catch (ClassNotFoundException var16) {
                        var16.printStackTrace();
                        lblNewLabel_10.setVisible(true);
                    }
                } else {
                    lblNewLabel_10.setVisible(true);
                }

                if (lblNewLabel_9.isVisible()) {
                    lblNewLabel_10.setVisible(false);
                }

            }
        });
        btnNewButton.setBounds(32, 275, 93, 23);
        this.contentPane.add(btnNewButton);
        JButton btnNewButton_1 = new JButton("返回");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AddBook.this.setVisible(false);
            }
        });
        btnNewButton_1.setBounds(308, 275, 93, 23);
        this.contentPane.add(btnNewButton_1);
        JLabel lblNewLabel_8 = new JLabel("分类");
        lblNewLabel_8.setBounds(32, 250, 54, 15);
        this.contentPane.add(lblNewLabel_8);
    }
}
