//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Testv1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class M_upPerson extends JFrame {
    private JPanel contentPane;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    M_upPerson frame = new M_upPerson();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public M_upPerson() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 488, 331);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JLabel lblNewLabel = new JLabel("姓名：");
        lblNewLabel.setFont(new Font("宋体", 0, 15));
        lblNewLabel.setBounds(43, 91, 54, 24);
        this.contentPane.add(lblNewLabel);
        JLabel lblNewLabel_1 = new JLabel("性别：");
        lblNewLabel_1.setFont(new Font("宋体", 0, 15));
        lblNewLabel_1.setBounds(43, 125, 54, 24);
        this.contentPane.add(lblNewLabel_1);
        JLabel lblNewLabel_2 = new JLabel("联系电话：");
        lblNewLabel_2.setFont(new Font("宋体", 0, 15));
        lblNewLabel_2.setBounds(37, 193, 88, 31);
        this.contentPane.add(lblNewLabel_2);
        JLabel lblNewLabel_3 = new JLabel("职称：");
        lblNewLabel_3.setFont(new Font("宋体", 0, 15));
        lblNewLabel_3.setBounds(43, 159, 54, 24);
        this.contentPane.add(lblNewLabel_3);
        this.textField = new JTextField();
        this.textField.setBounds(107, 93, 104, 21);
        this.contentPane.add(this.textField);
        this.textField.setColumns(10);
        this.textField_1 = new JTextField();
        this.textField_1.setBounds(107, 127, 66, 21);
        this.contentPane.add(this.textField_1);
        this.textField_1.setColumns(10);
        this.textField_2 = new JTextField();
        this.textField_2.setBounds(107, 162, 104, 21);
        this.contentPane.add(this.textField_2);
        this.textField_2.setColumns(10);
        this.textField_3 = new JTextField();
        this.textField_3.setBounds(117, 198, 148, 21);
        this.contentPane.add(this.textField_3);
        this.textField_3.setColumns(10);
        JButton btnNewButton = new JButton("返回");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                M_upPerson.this.setVisible(false);
            }
        });
        btnNewButton.setBounds(369, 259, 93, 23);
        this.contentPane.add(btnNewButton);
        final JLabel lblNewLabel_4 = new JLabel("请修改为男或女");
        lblNewLabel_4.setForeground(Color.RED);
        lblNewLabel_4.setBounds(218, 130, 122, 15);
        this.contentPane.add(lblNewLabel_4);
        lblNewLabel_4.setVisible(false);
        final JLabel lblNewLabel_5 = new JLabel("修改成功");
        lblNewLabel_5.setBounds(71, 234, 54, 15);
        this.contentPane.add(lblNewLabel_5);
        lblNewLabel_5.setVisible(false);
        JPanel panel = new JPanel();
        panel.setBounds(10, 10, 452, 75);
        this.contentPane.add(panel);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 46, 300, 150);
        panel.add(scrollPane);
        final String[] Name = new String[]{"工号", "姓名", "性别", "职称", "联系电话"};
        final Object[][] rowData = new Object[1][5];
        final JTable table = new JTable(rowData, Name);
        table.setBounds(297, 179, -279, -124);
        table.setRowHeight(30);
        table.getColumnModel().getColumn(0).setPreferredWidth(110);
        table.setPreferredScrollableViewportSize(new Dimension(440, 300));
        scrollPane.setViewportView(table);
        String num0 = null;

        try {
            String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
            String userName = "root";
            String userPwd = "ys124126";
            Connection dbConn = null;
            Statement state = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
            state = dbConn.createStatement();
            String sql0 = "SELECT 操作账号,时间 FROM 操作账号 ORDER BY 时间 DESC limit 1";
            ResultSet rs = state.executeQuery(sql0);

            String s1;
            for(s1 = new String(); rs.next(); s1 = rs.getString(1)) {
            }

            Statement stateCnt = dbConn.createStatement();
            String sql1 = "select * from 图书管理员 where 工号='" + s1 + "'";
            ResultSet rs_1 = stateCnt.executeQuery(sql1);

            for(int i = 0; rs_1.next() && i < rowData.length; ++i) {
                rowData[i][0] = rs_1.getString(1);
                rowData[i][1] = rs_1.getString(2);
                rowData[i][2] = rs_1.getString(3);
                rowData[i][3] = rs_1.getString(4);
                rowData[i][4] = rs_1.getString(5);
            }

            dbConn.close();
        } catch (SQLException var26) {
            var26.printStackTrace();
        } catch (ClassNotFoundException var27) {
            var27.printStackTrace();
        }

        JButton btnNewButton_1 = new JButton("保存");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = new String(M_upPerson.this.textField.getText());
                String work = new String(M_upPerson.this.textField_2.getText());
                String sex = new String(M_upPerson.this.textField_1.getText());
                String phone = new String(M_upPerson.this.textField_3.getText());
                new String();
                String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                String userName = "root";
                String userPwd = "ys124126";
                Connection dbConn = null;

                try {
                    Statement state = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    state = dbConn.createStatement();
                    String sql0 = "select 操作账号,时间 from 操作账号 order by 时间 desc limit 1";
                    ResultSet rs = state.executeQuery(sql0);

                    String s1;
                    for(s1 = new String(); rs.next(); s1 = rs.getString(1)) {
                    }

                    String num = s1;
                    PreparedStatement pst = null;
                    String sql;
                    if (!name.equals("")) {
                        sql = "update 图书管理员 set 姓名=? where 工号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, name);
                        pst.setString(2, s1);
                        pst.executeUpdate();
                    }

                    if (!sex.equals("")) {
                        try {
                            sql = "update 图书管理员 set 性别=? where 工号=?";
                            pst = dbConn.prepareStatement(sql);
                            pst.setString(1, sex);
                            pst.setString(2, num);
                            pst.executeUpdate();
                            lblNewLabel_4.setVisible(false);
                        } catch (Exception var21) {
                            var21.printStackTrace();
                            lblNewLabel_4.setVisible(true);
                            lblNewLabel_5.setVisible(false);
                        }
                    }

                    if (!work.equals("")) {
                        sql = "update 图书管理员 set 职称=? where 工号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, work);
                        pst.setString(2, s1);
                        pst.executeUpdate();
                    }

                    if (!phone.equals("")) {
                        sql = "update 图书管理员 set 联系电话=? where 工号=?";
                        pst = dbConn.prepareStatement(sql);
                        pst.setString(1, phone);
                        pst.setString(2, s1);
                        pst.executeUpdate();
                    }

                    Statement stateCnt = dbConn.createStatement();
                    String sql1 = "select * from 图书管理员 where 工号='" + s1 + "'";
                    ResultSet rs_1 = stateCnt.executeQuery(sql1);

                    for(int i = 0; rs_1.next() && i < rowData.length; ++i) {
                        rowData[i][0] = rs_1.getString(1);
                        rowData[i][1] = rs_1.getString(2);
                        rowData[i][2] = rs_1.getString(3);
                        rowData[i][3] = rs_1.getString(4);
                        rowData[i][4] = rs_1.getString(5);
                    }

                    TableModel tml = new DefaultTableModel(rowData, Name);
                    table.setModel(tml);
                    pst.close();
                    dbConn.close();
                    if (lblNewLabel_4.isVisible()) {
                        lblNewLabel_5.setVisible(false);
                    } else {
                        lblNewLabel_5.setVisible(true);
                    }
                } catch (SQLException var22) {
                    var22.printStackTrace();
                } catch (ClassNotFoundException var23) {
                    var23.printStackTrace();
                }

            }
        });
        btnNewButton_1.setBounds(43, 259, 93, 23);
        this.contentPane.add(btnNewButton_1);
    }
}
