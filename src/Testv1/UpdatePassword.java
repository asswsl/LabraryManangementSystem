//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Testv1;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

public class UpdatePassword extends JFrame {
    private JPanel contentPane;
    private JPasswordField passwordField;
    private JPasswordField passwordField_1;
    private JPasswordField passwordField_2;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UpdatePassword frame = new UpdatePassword();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public UpdatePassword() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 450, 300);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JLabel lblNewLabel = new JLabel("修改个人密码");
        lblNewLabel.setFont(new Font("宋体", 0, 20));
        lblNewLabel.setBounds(144, 10, 140, 31);
        this.contentPane.add(lblNewLabel);
        JButton btnNewButton = new JButton("返回");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UpdatePassword.this.setVisible(false);
            }
        });
        btnNewButton.setBounds(331, 228, 93, 23);
        this.contentPane.add(btnNewButton);
        JLabel lblNewLabel_1 = new JLabel("原密码");
        lblNewLabel_1.setBounds(31, 57, 54, 15);
        this.contentPane.add(lblNewLabel_1);
        JLabel lblNewLabel_2 = new JLabel("新密码");
        lblNewLabel_2.setBounds(31, 108, 54, 15);
        this.contentPane.add(lblNewLabel_2);
        JLabel lblNewLabel_3 = new JLabel("确认密码");
        lblNewLabel_3.setBounds(10, 151, 54, 15);
        this.contentPane.add(lblNewLabel_3);
        final JLabel lblNewLabel_4 = new JLabel("输入错误，请校检");
        lblNewLabel_4.setForeground(Color.RED);
        lblNewLabel_4.setBounds(90, 192, 225, 15);
        this.contentPane.add(lblNewLabel_4);
        lblNewLabel_4.setVisible(false);
        this.passwordField = new JPasswordField();
        this.passwordField.setBounds(78, 54, 237, 23);
        this.contentPane.add(this.passwordField);
        this.passwordField_1 = new JPasswordField();
        this.passwordField_1.setBounds(78, 105, 237, 23);
        this.contentPane.add(this.passwordField_1);
        this.passwordField_2 = new JPasswordField();
        this.passwordField_2.setBounds(78, 151, 237, 23);
        this.contentPane.add(this.passwordField_2);
        final JLabel lblNewLabel_5 = new JLabel("修改成功");
        lblNewLabel_5.setBounds(31, 206, 54, 15);
        this.contentPane.add(lblNewLabel_5);
        lblNewLabel_5.setVisible(false);
        JButton btnNewButton_1 = new JButton("确认");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String s1 = new String(String.valueOf(UpdatePassword.this.passwordField.getPassword()));
                String s2 = new String(String.valueOf(UpdatePassword.this.passwordField_1.getPassword()));
                String s3 = new String(String.valueOf(UpdatePassword.this.passwordField_2.getPassword()));
                String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
                String userName = "root";
                String userPwd = "ys124126";
                Connection dbConn = null;
                String sql = "select 工号,密码 from 图书管理员";
                String sql1 = "select 借书证号,密码 from 借书者";
                new String();

                try {
                    Statement state = null;
                    Statement state0 = null;
                    Statement state1 = null;
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
                    state = dbConn.createStatement();
                    state0 = dbConn.createStatement();
                    state1 = dbConn.createStatement();
                    ResultSet rs = state.executeQuery(sql);
                    ResultSet rs0 = state0.executeQuery(sql1);
                    String sql0 = "select top 1* from 操作账号 order by 操作账号 desc";
                    ResultSet rs1 = state1.executeQuery(sql0);

                    String s10;
                    for(s10 = new String(); rs1.next(); s10 = rs1.getString(1)) {
                    }

                    String num = s10;
                    PreparedStatement pst = null;

                    while(true) {
                        String tmp;
                        String tmp_1;
                        String sql_1;
                        while(rs.next()) {
                            tmp = rs.getString(1);
                            tmp_1 = rs.getString(2);
                            if (tmp.equals(num) && tmp_1.equals(s1)) {
                                if (s2.equals(s3)) {
                                    sql_1 = "update 图书管理员 set 密码=? where 工号=?";
                                    pst = dbConn.prepareStatement(sql_1);
                                    pst.setString(1, s3);
                                    pst.setString(2, num);
                                    pst.executeUpdate();
                                    lblNewLabel_5.setVisible(true);
                                    if (lblNewLabel_5.isVisible()) {
                                        lblNewLabel_4.setVisible(false);
                                    }
                                } else {
                                    lblNewLabel_4.setVisible(true);
                                }
                            } else {
                                lblNewLabel_4.setVisible(true);
                            }
                        }

                        while(true) {
                            while(rs0.next()) {
                                tmp = rs0.getString(1);
                                tmp_1 = rs0.getString(2);
                                if (tmp.equals(num) && tmp_1.equals(s1)) {
                                    if (s2.equals(s3)) {
                                        sql_1 = "update 借书者 set 密码=? where 借书证号=?";
                                        pst = dbConn.prepareStatement(sql_1);
                                        pst.setString(1, s3);
                                        pst.setString(2, num);
                                        pst.executeUpdate();
                                        lblNewLabel_5.setVisible(true);
                                        lblNewLabel_4.setVisible(false);
                                    } else {
                                        lblNewLabel_4.setVisible(true);
                                    }
                                } else {
                                    lblNewLabel_4.setVisible(true);
                                }
                            }

                            if (lblNewLabel_5.isVisible()) {
                                lblNewLabel_4.setVisible(false);
                            }

                            dbConn.close();
                            return;
                        }
                    }
                } catch (SQLException var24) {
                    var24.printStackTrace();
                } catch (ClassNotFoundException var25) {
                    var25.printStackTrace();
                }

            }
        });
        btnNewButton_1.setBounds(10, 228, 93, 23);
        this.contentPane.add(btnNewButton_1);
    }
}
