//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Testv1;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

public class Retrun_table extends JFrame {
    private JPanel contentPane;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Retrun_table frame = new Retrun_table();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public Retrun_table() {
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 720, 367);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout((LayoutManager)null);
        JPanel panel = new JPanel();
        panel.setBounds(10, 10, 684, 290);
        this.contentPane.add(panel);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 46, 300, 150);
        panel.add(scrollPane);
        String[] Name = new String[]{"借书证号", "姓名", "书号", "书名", "借书时间", "还书时间"};
        Object[][] rowData = new Object[100][6];
        JTable table = new JTable(rowData, Name);
        table.setBounds(297, 179, -279, -124);
        table.setRowHeight(30);
        table.getColumnModel().getColumn(0).setPreferredWidth(50);
        table.setPreferredScrollableViewportSize(new Dimension(650, 300));
        scrollPane.setViewportView(table);

        try {
            String dbURL = "jdbc:mysql://localhost:3306/librarymanagement";
            String userName = "root";
            String userPwd = "ys124126";
            Connection dbConn = null;
            Statement state = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
            state = dbConn.createStatement();
            String sql0 = "select * from 还书 order by 还书时间 asc";
            ResultSet rs = state.executeQuery(sql0);

            for(int i = 0; rs.next() && i < rowData.length; ++i) {
                rowData[i][0] = rs.getString(1);
                rowData[i][1] = rs.getString(2);
                rowData[i][2] = rs.getString(3);
                rowData[i][3] = rs.getString(4);
                rowData[i][4] = rs.getString(5);
                rowData[i][5] = rs.getString(6);
            }

            dbConn.close();
        } catch (SQLException var14) {
            var14.printStackTrace();
        } catch (ClassNotFoundException var15) {
            var15.printStackTrace();
        }

        JButton btnNewButton = new JButton("返回");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Retrun_table.this.setVisible(false);
            }
        });
        btnNewButton.setBounds(431, 305, 93, 23);
        this.contentPane.add(btnNewButton);
    }
}
